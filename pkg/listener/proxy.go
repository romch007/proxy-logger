package listener

import (
	"net/http"

	"github.com/elazarl/goproxy"
	"gitlab.com/romch007/proxy-logger/pkg/logger"
)

func StartProxy(address string) error {
	proxy := goproxy.NewProxyHttpServer()
	proxy.OnRequest().DoFunc(onRequest)
	return http.ListenAndServe(address, proxy)
}

func onRequest(r *http.Request, ctx *goproxy.ProxyCtx) (*http.Request, *http.Response) {
	go logger.Log(ctx.Req)
	return r, nil
}
