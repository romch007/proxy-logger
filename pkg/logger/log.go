package logger

import (
	"bytes"
	"log"
	"net/http"
	"text/template"
)

func Log(request *http.Request) error {
	tmpl, err := template.New("request").Parse("{{.Method}} {{.URL}}")
	if err != nil {
		return err
	}
	buf := &bytes.Buffer{}
	tmpl.Execute(buf, request)
	log.Println(buf.String())
	return nil
}
