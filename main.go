package main

import (
	"gitlab.com/romch007/proxy-logger/pkg/listener"
	"log"
)

func main() {
	log.Fatal(listener.StartProxy(":8080"))
}
